FROM ubuntu:22.04

LABEL maintainer="cschu1981@gmail.com"
LABEL version="1.2"
LABEL description="This is a Docker Image for the gapseq tool."

# https://github.com/jotech/gapseq

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

# Installation of main system dependencies
RUN apt install -y locales ncbi-blast+ wget git libglpk-dev r-base-core exonerate bedtools barrnap bc parallel curl libcurl4-openssl-dev libssl-dev libsbml5-dev bc

RUN apt clean

RUN locale-gen en_US en_US.UTF-8 && dpkg-reconfigure locales

# installation of required R-packages
RUN R -e 'install.packages(c("data.table", "stringr", "getopt", "doParallel", "foreach", "R.utils", "stringi", "glpkAPI", "CHNOSZ", "jsonlite", "httr"))' && \
    R -e 'if (!requireNamespace("BiocManager", quietly = TRUE)) install.packages("BiocManager"); BiocManager::install("Biostrings")' && \
    wget https://cran.r-project.org/src/contrib/Archive/sybil/sybil_2.2.0.tar.gz && \
    wget https://cran.r-project.org/src/contrib/Archive/sybilSBML/sybilSBML_3.1.2.tar.gz && \
    R CMD INSTALL sybil_2.2.0.tar.gz && \
    R CMD INSTALL sybilSBML_3.1.2.tar.gz && \
    rm sybil_2.2.0.tar.gz && \
    rm sybilSBML_3.1.2.tar.gz

# Download latest gapseq version from github
# Download latest reference sequence database

RUN mkdir -p /opt/software && \
    cd /opt/software && \
    git clone -b feature/allow_external_database_20240821 https://github.com/cschu/gapseq 
    #git clone https://github.com/jotech/gapseq && \
    #cd gapseq && \
    #sed -i "394d;395d;396d" ./src/gapseq_find.sh && \
    #sed -i "80i     echo \"SEQDIR=\$GAPSEQ_SEQ_DIR\"" ./src/gapseq_find.sh

    # bash ./src/update_sequences.sh Bacteria && \
    # bash ./src/update_sequences.sh Archaea && \
    # find dat -exec chmod 755 {} \; && \

ENV PATH /opt/software/gapseq:/opt/software/gapseq/src:$PATH
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8


# trigger trigger

